/*
 * Copyright (c) 2020 Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "str.hpp"
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>

char toLowerCase(char c)
{
    if( c >= 'A' && c <='Z')
    {
        c += 'a' - 'A';
    }
    return c;
}

bool icaseCompare(char lhv, char rhv)
{
    return toLowerCase(lhv) < toLowerCase(rhv);
}

using String = task::String;

void sortLines (std::vector<String>& vec)
{
    auto cmp    = [](const String& lhv, const String& rhv)
    {
        return std::lexicographical_compare(rhv.begin(), rhv.end(),
                                            lhv.begin(), lhv.end(),
                                            icaseCompare);
    };

    std::sort(vec.begin(), vec.end(), cmp);

}

std::vector<String> readFile(const char* path)
{
    // NRVO
    std::vector<String> strings;

    std::ifstream file(path);

    if(file.is_open())
    {
        while(!file.eof())
        {
            // I want to keep String class minimalistic.
            // No additional methods, no push_back, etc...
            char buff[256];
            file.getline(buff, sizeof(buff));
            std::size_t sz  = file.gcount();

            // delimiter is counted
            // skip delimiter
            if( !file.fail() ) --sz;
            if(sz)
            {
                String str(buff, sz);

                // the line is longer than the buffer
                while (file.fail() && !file.eof())
                {
                    file.clear( file.rdstate() & ~std::ios_base::failbit);

                    file.getline(buff, sizeof(buff));
                    sz  = file.gcount();

                    // skip delimiter
                    if( !file.fail() ) --sz;

                    if(sz)
                    {
                        str += String(buff, sz);
                    }
                }

                strings.push_back(std::move(str));
            }
        }
    } else {
        std::cerr << "can't open " << path << std::endl;
    }

    return strings;
}

void print(const std::vector<String>& strings)
{
    for(auto&& str: strings)
    {
        std::cout.write(str.data(), str.size());
        std::cout<< std::endl;
    }
}

int main(int argc, char** argv){
    using std::cout;
    using std::endl;

    const char usage_str[]  = "targem_task [input_file]";
    const char def_input[]  = "input.txt";
    const char* input   = (argc > 1)? argv[1]: def_input;

    if( argc == 1){
        cout << "usage: " << usage_str << endl
                  << endl
                  << "reading \"" << def_input << "\"" << endl
                  << endl;
    }

    auto strs   = readFile(input);
    sortLines(strs);
    print(strs);
    return 0;
}
