/*
 * Copyright (c) 2020 Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "str.hpp"
#include <algorithm>

namespace task {

namespace{

// I don't want to include cstring
String::Size strLen(const String::Char* cstr)
{
    String::Size len = 0;
    for(; cstr[len] != String::Char{}; ++len)
    {}
    return len;
}

}

String::String(const Char* cstr):
    String(cstr, strLen(cstr))
{}

String::String(const Char* cstr, Size len)
{
    copy(cstr, len);
}

String::String(const String& other):
    String(other.data(), other.size())
{}

String::String(String&& other) noexcept:
    m_data(other.m_data),
    m_size(other.m_size),
    m_capacity(other.m_capacity)
{
    other.reset();
}

String& String::operator = (const String& other)
{
    if( this != &other)
    {
        copy(other.data(), other.size());
    }
    return *this;
}

String& String::operator = (String&& other) noexcept
{
    if( this != &other)
    {
        deallocate(m_data);

        m_data      = other.data();
        m_size      = other.size();
        m_capacity  = other.capacity();

        other.reset();
    }
    return *this;
}

String& String::operator += (const String& other)
{
    Size newSize   = size() + other.size();
    if( newSize > capacity() )
    {
        changeCapacity(newSize, true);
    }

    std::copy(other.begin(), other.end(), end());
    m_size  = newSize;
    return *this;
}

void String::reserve(String::Size new_capacity)
{
    if(new_capacity > capacity())
    {
        changeCapacity(new_capacity, true);
    }
}

String::~String()
{
    deallocate(m_data);
}

String::Char* String::allocate(Size sz)
{
    return new Char[sz];
}

void String::deallocate(Char* ptr) noexcept
{
    delete[] ptr;
}

void String::changeCapacity(Size newCapacity, bool preserveContent)
{
    Char* newData  = allocate(newCapacity);

    if(preserveContent)
    {
        Size sz    = std::min(newCapacity, size());
        std::copy(m_data, m_data + sz, newData);
    }

    deallocate(m_data);

    m_data      = newData;
    m_capacity  = newCapacity;

    if( size() > capacity() )
    {
        m_size  = capacity();
    }
}

void String::copy(const Char* first, Size count)
{
    if(count > capacity())
    {
        changeCapacity(count, false);
    }

    std::copy(first, first + count, m_data);
    m_size  = count;
}

void String::reset()
{
    m_data      = nullptr;
    m_capacity  = 0;
    m_size      = 0;
}

String operator +(const String& lhv, String&& rhv)
{
    rhv.reserve(lhv.size() + rhv.size());
    rhv += lhv;
    std::rotate(rhv.begin(), rhv.begin() + rhv.size() - lhv.size(), rhv.end() );
    return std::move(rhv);
}

String operator +(String&& lhv, const String& rhv)
{
    lhv += rhv;
    return std::move(lhv);
}

String operator +(String&& lhv, String&& rhv)
{
    return std::move(lhv) + rhv;
}

} //  namespace task
