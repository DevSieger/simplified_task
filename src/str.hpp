/*
 * Copyright (c) 2020 Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <cstddef>

namespace task {

// The task says nothing about a character type of the string.
// So I free to make it simple class, not a template.
class String
{
public:
    using Size  = std::size_t;
    using Char  = char;

    String() = default;
    String(const Char* cstr);
    String(const Char* cstr, Size len);

    String(const String& other);
    String(String&& other) noexcept;

    String& operator = (const String& other);
    String& operator = (String&& other) noexcept;

    String& operator +=(const String& other);

    Char*       data()          { return m_data;}
    const Char* data()  const   { return m_data;}

    Char*       begin()         { return data(); }
    const Char* begin() const   { return data(); }
    const Char* cbegin() const  { return begin();}

    Char*       end()           { return begin() + size(); }
    const Char* end() const     { return begin() + size(); }
    const Char* cend() const    { return end(); }

    Size size() const noexcept
    {
        return m_size;
    }

    Size capacity() const noexcept
    {
        return m_capacity;
    }

    void reserve(Size new_capacity);

    ~String();

private:
    Char*   allocate(Size sz);

    // Ignores nullptr
    void    deallocate(Char* ptr) noexcept;

    void    changeCapacity(Size newCapacity, bool preserveContent);

    // Copies [first, first + count) to the string, discarding any previous content.
    // The string grows when necessary
    void    copy(const Char* first, Size count);

    // Reset the state of the string (when it was moved) to the default constructed.
    void    reset();

    Char*   m_data      = nullptr;
    Size    m_size      = 0;
    Size    m_capacity  = 0;
};

inline String operator + (const String& lhv, const String& rhv)
{
    // NRVO
    String tmp;
    tmp.reserve(lhv.size() + rhv.size());
    tmp += lhv;
    tmp += rhv;
    return tmp;
}

String operator + (const String& lhv, String&& rhv);
String operator + (String&& lhv, const String& rhv);
String operator + (String&& lhv, String&& rhv);

} //  namespace task
